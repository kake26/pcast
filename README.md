# Pcast

PHP for playing back asciinema recorded sessions via the standalone player. Intended for easily handling multiple different screencasts for play back. 

# Dependencies

asciinema standalone player (resides in the /player directory) - https://github.com/asciinema/asciinema-player

Asciinema Copyright and License info:

Copyright © 2011-2021 Marcin Kulik.

All code is licensed under the Apache License, Version 2.0.

